﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;

using VRageMath;
using VRage.Game;
using VRage.Collections;
using Sandbox.ModAPI.Ingame;
using VRage.Game.Components;
using VRage.Game.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using Sandbox.Game.EntityComponents;
using SpaceEngineers.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;

namespace SpaceEngineers.SolarPanelAlign
{
    public sealed class SolarPanelAlign : MyGridProgram
    {
        //
        // REQUIREMENTS:
        // - TextPanel -- "Debug Panel"
        // - TimerBlock -- "Angle Timer"
        //   = This should be set for 00:02
        //   = Argument should be "Check Angle"
        // - TimerBlock -- "Check Timer"
        //   = Set this to however long you want to wait after the last alignment before checking again
        //   = Argument should be "Update Output"
        // - BasicRotor -- "Base Rotor"
        //   = This is the rotor at the base of the arm that rotates the whole array on the X-Axis
        // - BasicRotor -- "Arm1 Rotor"
        // - BasicRotor -- "Arm2 Rotor"
        //   = These are the rotors to adjust the solar panels on the Y-Axis
        // - Group< SolarPanel > -- "Arm1 Solar Panels"
        // - Group< SolarPanel > -- "Arm2 Solar Panels"
        //   = These are the solar panels on each arm grouped together
        //   = NOTE: I base my alignment on maximum output per-arm so this is critical
        //

        private float Last_Stop;
        private float Best_Power;

        private bool Base_Found;
        private bool Reverse;
        private bool Reverse_Pause;
        private bool Arm1_Found;

        public SolarPanelAlign()
        {
            Last_Stop = 100;    // Start immediately
        }

        public void Main(string argument)
        {
            IMyMotorStator Base_Rotor = GridTerminalSystem.GetBlockWithName("Base Rotor") as IMyMotorStator;
            IMyTimerBlock Angle_Timer = GridTerminalSystem.GetBlockWithName("Angle Timer") as IMyTimerBlock;
            IMyTimerBlock Check_Timer = GridTerminalSystem.GetBlockWithName("Check Timer") as IMyTimerBlock;

            float Total_Power;

            Debug("", true);

            //  
            // Calculate the total power for the arms  
            //  
            {
                List<IMyTerminalBlock> blocks = new List<IMyTerminalBlock>();

                float Arm1_Power;
                float Arm2_Power;

                GridTerminalSystem.GetBlockGroupWithName("Arm1 Solar Panels").GetBlocksOfType<IMySolarPanel>(blocks);
                Arm1_Power = 0;
                for (int i = 0; i < blocks.Count; ++i)
                {
                    IMySolarPanel solar = blocks[i] as IMySolarPanel;

                    Arm1_Power += solar.CurrentOutput;
                }

                GridTerminalSystem.GetBlockGroupWithName("Arm2 Solar Panels").GetBlocksOfType<IMySolarPanel>(blocks);
                Arm2_Power = 0;
                for (int i = 0; i < blocks.Count; ++i)
                {
                    IMySolarPanel solar = blocks[i] as IMySolarPanel;

                    Arm2_Power += solar.CurrentOutput;
                }

                Total_Power = Arm1_Power + Arm2_Power;

                Debug("Arm1_Output: " + Arm1_Power);
                Debug("Arm2_Output: " + Arm2_Power);
                Debug("Total_Output: " + Total_Power);
                Debug("Best_Ever: " + Last_Stop);
            }

            if (argument == "Update Output")
            {
                if (Total_Power < Last_Stop)  
                {
                    Debug("Starting aglignment.");

                    Base_Found = false;
                    Arm1_Found = false;
                    Best_Power = Total_Power;

                    Reverse = false;
                    Reverse_Pause = true;
                    Angle_Timer.GetActionWithName("Start").Apply(Angle_Timer);
                }
                else
                {
                    Debug("Ideal Output! Last_Stop = " + Last_Stop);

                    Check_Timer.GetActionWithName("Start").Apply(Check_Timer);
                }
            }
            else if (argument == "Check Angle")
            {
                IMyMotorStator motor;

                Debug("Executing Check Angle.");
                Angle_Timer.GetActionWithName("Start").Apply(Angle_Timer);

                //
                // Select the correct motor first
                //
                if (Base_Found == false)
                {
                    Debug("Updating Base Angle");
                    motor = GridTerminalSystem.GetBlockWithName("Base Rotor") as IMyMotorStator;
                }
                else if (Arm1_Found == false)
                {
                    Debug("Updating Arm 1 Angle");
                    motor = GridTerminalSystem.GetBlockWithName("Arm1 Rotor") as IMyMotorStator;
                }
                else
                {
                    Debug("Updating Arm 2 Angle");
                    motor = GridTerminalSystem.GetBlockWithName("Arm2 Rotor") as IMyMotorStator;
                }

                //
                // Alignment state machine
                //
                motor.GetActionWithName("OnOff_On").Apply(motor);
                if (Reverse_Pause == false)
                {
                    Debug("Reverse Pause");
                    Reverse_Pause = true;
                }
                else if (Total_Power > Best_Power)
                {
                    Debug("Seeking -- (" + Total_Power + ") > (" + Best_Power + ")");

                    Best_Power = Total_Power;
                }
                else if (Reverse == false)
                {
                    Debug("Reversing -- (" + Total_Power + ") <= (" + Best_Power + ")");

                    Reverse = true;
                    Reverse_Pause = false;
                    motor.GetActionWithName("Reverse").Apply(motor);
                }
                else
                {
                    Debug("Next Motor -- (" + Total_Power + ") <= (" + Best_Power + ")");

                    Reverse = false;
                    Reverse_Pause = false;
                    motor.GetActionWithName("OnOff_Off").Apply(motor);

                    if (Base_Found == false)
                    {
                        Base_Found = true;
                    }
                    else if (Arm1_Found == false)
                    {
                        Arm1_Found = true;
                    }
                    else
                    {
                        Debug("Done for now!");
                        Last_Stop = Total_Power;

                        Check_Timer.GetActionWithName("Start").Apply(Check_Timer);
                        Angle_Timer.GetActionWithName("Stop").Apply(Angle_Timer);
                    }
                }
            }
            else
            {
                Debug("Debug Run");
            }
        }

        private void Debug(string str, bool init = false)
        {
            IMyTextPanel output = GridTerminalSystem.GetBlockWithName("Debug Panel") as IMyTextPanel;

            output.WritePublicText(str + "\n", !init);
        }

        public void Save()
        {
        }
    }
}