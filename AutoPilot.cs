﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;

using VRageMath;
using VRage.Game;
using VRage.Collections;
using Sandbox.ModAPI.Ingame;
using VRage.Game.Components;
using VRage.Game.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using Sandbox.Game.EntityComponents;
using SpaceEngineers.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;

namespace SpaceEngineers.AutoPilot
{
    public sealed class Program : MyGridProgram
    {
        //=======================================================================
        //////////////////////////BEGIN//////////////////////////////////////////
        //=======================================================================

        public Program()
        {
        }

        public void Main(string args)
        {
            //
            // Get my relative points
            //
            IMyTerminalBlock nose = GridTerminalSystem.GetBlockWithName("Camera Front");
            IMyGyro gyro = GridTerminalSystem.GetBlockWithName("Gyroscope") as IMyGyro;
            IMyTimerBlock timer = GridTerminalSystem.GetBlockWithName("Timer Block") as IMyTimerBlock;

            Vector3D target = new Vector3D(0, 0, 0);
            
            Echo("Center Position: " + gyro.GetPosition());
            Echo("Nose Position:  " + nose.GetPosition());
            Echo("\n");
            Echo("Ship Vector: " + (nose.GetPosition() - gyro.GetPosition()));
            Echo("TargetVector: " + (target - gyro.GetPosition()));
        }

        public void Save()
        {
        }

        //=======================================================================
        //////////////////////////END////////////////////////////////////////////
        //=======================================================================
    }
}