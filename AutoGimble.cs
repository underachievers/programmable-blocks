﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;

using VRageMath;
using VRage.Game;
using VRage.Collections;
using Sandbox.ModAPI.Ingame;
using VRage.Game.Components;
using VRage.Game.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using Sandbox.Game.EntityComponents;
using SpaceEngineers.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;

namespace SpaceEngineers.AutoGimble
{
    public sealed class  Program : MyGridProgram
    {
        enum Commands
        {
            Stop = 0,
            Stabilize
        }

        private MyNavigation NavBlock;
        private IMyTimerBlock Timer;
        private Commands Command;
        private int TickCount;

        //
        // Control Variables
        //
        string ShipName = "Atmos Miner";
        int Clock = 10;
        float DrillGyroMult = 2f;

        public Program()
        {
        }

        public void Main(string args)
        {
            if (NavBlock == null)
                NavBlock = new MyNavigation(this);
            if (Timer == null)
                Timer = GridTerminalSystem.GetBlockWithName("Timer Block") as IMyTimerBlock;

            TickCount++;

            switch (args)
            {
                case "Start":
                    {
                        Command = Commands.Stabilize;
                        break;
                    }

                case "Stop":
                    {
                        Command = Commands.Stop;
                        break;
                    }

                case "Calibrate":
                    {
                        NavBlock.Calibrate();
                        break;
                    }
            }

            if (Command != Commands.Stop)
            {
                if ((TickCount % Clock) == 0)
                {
                    NavBlock.Update();
                    NavBlock.Drill();
                }
                Timer.GetActionWithName("TriggerNow").Apply(Timer);
            }
        }

        public void SetGyroOverrides(bool OverrideOnOff, Vector3D settings, float Power = 1)
        {
            var gyros = new List<IMyTerminalBlock>();
            GridTerminalSystem.GetBlocksOfType<IMyGyro>(gyros);
            for (int i = 0; i < gyros.Count; ++i)
            {
                IMyGyro gyro = gyros[i] as IMyGyro;
                if (gyro != null)
                {
                    if ((!gyro.GyroOverride) && OverrideOnOff)
                        gyro.ApplyAction("Override");
                    gyro.SetValue("Power", Power);
                    gyro.SetValue("Yaw", settings.GetDim(0));
                    gyro.SetValue("Pitch", settings.GetDim(1));
                    gyro.SetValue("Roll", settings.GetDim(2));
                }
            }
        }

        public class MyNavigation
        {
            private Program ParentProgram;
            private IMyRemoteControl RemCon;

            public MyNavigation(Program program)
            {
                ParentProgram = program;
                RemCon = program.GridTerminalSystem.GetBlockWithName("Remote Control") as IMyRemoteControl;
            }

            public Vector3D ForwVelocityVector { get; private set; }
            public Vector3D GravVector { get; private set; }
            public Vector3D LeftVelocityVector { get; private set; }
            public Vector3D MyPos { get; private set; }
            public Vector3D MyPrevPos { get; private set; }
            public Vector3D VelocityVector { get; private set; }
            public MatrixD DrillMatrix { get; private set; }

            public void Update()
            {
                MyPrevPos = MyPos;
                MyPos = RemCon.GetPosition();
                GravVector = RemCon.GetNaturalGravity();
                VelocityVector = (MyPos - MyPrevPos) * 60 / ParentProgram.Clock;
                ForwVelocityVector = RemCon.WorldMatrix.Forward * Vector3D.Dot(VelocityVector, RemCon.WorldMatrix.Forward);
                LeftVelocityVector = RemCon.WorldMatrix.Left * Vector3D.Dot(VelocityVector, RemCon.WorldMatrix.Left);
            }

            public void Drill()
            {
                float maxLSpeed;
                float maxFSpeed;

                Vector3D myPosDrill = Vector3D.Transform(MyPos, DrillMatrix); // - DrillPoint;

                double shiftX = myPosDrill.GetDim(0) / 2;
                if (shiftX < 0)
                    shiftX = Math.Max(shiftX, -0.1);
                else
                    shiftX = Math.Min(shiftX, 0.1);

                double shiftZ = myPosDrill.GetDim(2) / 2;
                if (shiftZ < 0)
                    shiftZ = Math.Max(shiftZ, -0.1);
                else
                    shiftZ = Math.Min(shiftZ, 0.1);

                Vector3D gyroAng = GetNavAngles(myPosDrill + new Vector3D(0, 0, 1), DrillMatrix, shiftX, shiftZ);
                ParentProgram.SetGyroOverrides(true, gyroAng * ParentProgram.DrillGyroMult, 1);

                maxLSpeed = (float)Math.Sqrt(2 * Math.Abs(myPosDrill.GetDim(0)) * ParentProgram.ThrustBlock.MaxAccel) / 5;
                maxFSpeed = (float)Math.Sqrt(2 * Math.Abs(myPosDrill.GetDim(2)) * ParentProgram.ThrustBlock.MaxAccel) / 5;

                if (LeftVelocityVector.Length() < maxLSpeed)
                    ParentProgram.ThrustBlock.SetOverrideAccel("R", (float)(myPosDrill.GetDim(0) * 10));
                else
                {
                    ParentProgram.ThrustBlock.SetOverridePercent("R", 0);
                    ParentProgram.ThrustBlock.SetOverridePercent("L", 0);
                }

                if (ForwVelocityVector.Length() < maxFSpeed)
                    ParentProgram.ThrustBlock.SetOverrideAccel("B", (float)(myPosDrill.GetDim(2) * 10));
                else
                {
                    ParentProgram.ThrustBlock.SetOverridePercent("F", 0);
                    ParentProgram.ThrustBlock.SetOverridePercent("B", 0);
                }
            }

            private Vector3D GetNavAngles(Vector3D target, MatrixD invMatrix, double shiftX = 0, double shiftZ = 0)
            {
                Vector3D V3Dcenter = RemCon.GetPosition();
                Vector3D V3Dfow = RemCon.WorldMatrix.Forward + V3Dcenter;
                Vector3D V3Dup = RemCon.WorldMatrix.Up + V3Dcenter;
                Vector3D V3Dleft = RemCon.WorldMatrix.Left + V3Dcenter;
                Vector3D gravNorm = Vector3D.Normalize(GravVector) + V3Dcenter;

                V3Dcenter = Vector3D.Transform(V3Dcenter, invMatrix);
                V3Dfow = Vector3D.Transform(V3Dfow, invMatrix) - V3Dcenter;
                V3Dup = Vector3D.Transform(V3Dup, invMatrix) - V3Dcenter;
                V3Dleft = Vector3D.Transform(V3Dleft, invMatrix) - V3Dcenter;
                gravNorm = Vector3D.Normalize(Vector3D.Transform(gravNorm, invMatrix) - V3Dcenter - new Vector3D(shiftX, 0, shiftZ));

                Vector3D targetNorm = Vector3D.Normalize(Vector3D.Reject(target - V3Dcenter, gravNorm));

                double targetPitch = Vector3D.Dot(V3Dfow, Vector3D.Normalize(Vector3D.Reject(-gravNorm, V3Dleft)));
                targetPitch = Math.Acos(targetPitch) - Math.PI / 2;

                double targetRoll = Vector3D.Dot(V3Dleft, Vector3D.Reject(-gravNorm, V3Dfow));
                targetRoll = Math.Acos(targetRoll) - Math.PI / 2;

                double targetYaw = Math.Acos(Vector3D.Dot(V3Dfow, targetNorm));
                if ((V3Dleft - targetNorm).Length() < Math.Sqrt(2))
                    targetYaw = -targetYaw;

                if (double.IsNaN(targetYaw))
                    targetYaw = 0;
                if (double.IsNaN(targetPitch))
                    targetPitch = 0;
                if (double.IsNaN(targetRoll))
                    targetRoll = 0;

                return new Vector3D(targetYaw, targetPitch, targetRoll);
            }

            internal void Calibrate()
            {
                MatrixD mRot;
                Vector3D V3Dcenter = RemCon.GetPosition();
                Vector3D V3Dup = -Vector3D.Normalize(RemCon.GetNaturalGravity());
                Vector3D V3Dleft = Vector3D.Normalize(Vector3D.Reject(RemCon.WorldMatrix.Left, V3Dup));
                Vector3D V3Dfow = Vector3D.Normalize(Vector3D.Cross(V3Dleft, V3Dup));

                mRot = new MatrixD(V3Dleft.GetDim(0), V3Dleft.GetDim(1), V3Dleft.GetDim(2), 0, V3Dup.GetDim(0), V3Dup.GetDim(1), V3Dup.GetDim(2), 0, V3Dfow.GetDim(0), V3Dfow.GetDim(1), V3Dfow.GetDim(2), 0, 0, 0, 0, 1);
                mRot = MatrixD.Invert(mRot);
                DrillMatrix = new MatrixD(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, -V3Dcenter.GetDim(0), -V3Dcenter.GetDim(1), -V3Dcenter.GetDim(2), 1) * mRot;
            }
        }
    }
}