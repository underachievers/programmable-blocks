﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;

using VRageMath;
using VRage.Game;
using VRage.Collections;
using Sandbox.ModAPI.Ingame;
using VRage.Game.Components;
using VRage.Game.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using Sandbox.Game.EntityComponents;
using SpaceEngineers.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;

namespace SpaceEngineers.TriggerTesting
{
    public sealed class Program : MyGridProgram
    {
        private int arbitraryValue;

        public Program()
        {
            arbitraryValue = 0;
        }

        public void Main(string args)
        {
            IMyTextPanel debug = GridTerminalSystem.GetBlockWithName("Debug2") as IMyTextPanel;
            IMyTimerBlock timer = GridTerminalSystem.GetBlockWithName("DebugTimer") as IMyTimerBlock;
            IMyOreDetector detect = GridTerminalSystem.GetBlockWithName("Ore Detector") as IMyOreDetector;

            arbitraryValue += 1;

            debug.WritePublicText("" + arbitraryValue);

            timer.GetActionWithName("TriggerNow").Apply(timer);
            Echo("" + timer.GetPosition());
        }

        public void Save()
        {
        }
    }
}