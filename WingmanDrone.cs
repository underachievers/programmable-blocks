﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;

using VRageMath;
using VRage.Game;
using VRage.Collections;
using Sandbox.ModAPI.Ingame;
using VRage.Game.Components;
using VRage.Game.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using Sandbox.Game.EntityComponents;
using SpaceEngineers.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;

namespace SpaceEngineers.WingmanDrone
{
    public sealed class Program : MyGridProgram
    {
        /*WingmanDrone with Fixed Weapon by Spiritplumber based on Sunoko*/

        // ship needs: remote control named as below, sensor named as below, a forward facing gun OR a turret that can also face forward (can have more turrets), optionally a display.

        // settings
        int SafeDistanceEnemies = 40; // If closer than this to target, turn collision avoidance on. if set to zero, OK to kamikaze run / ram if out of weapons
        int SafeDistanceLeader = 40; // If closer than this to target, turn collision avoidance on.
        int MinDistanceLeader = 30; // If close than this to leader, turn the autopilot off entirely.
        int SafeDistanceOrigin = 200; // If closer than this to origin, turn collision avoidance on.
        int GoodEnoughAngle = 10; // if less than this, fire forward facing weapons
        int GoHomeDistance = 20000; // if target is this far away from home, abort regardless
        int RejoinLeaderDistance = 6000; // if you're this far from the wingleader, rejoin leader even if you are still in a fight
        int ScoutAheadTime = 120; // in ticks. When given the "scout ahead" order, come back after this long
        int StayPutTime = 120; // in ticks. When given the "stay put" order, come back after this long
        Orders DefaultOrder = Orders.ATTACK_ENEMY; // what to default to once we're done executing a timed order
        String remoteName = "AI Remote"; //required: name of remote
        String sensorEName = "AI Sensor Enemy"; //required for non-NPC operation
        String sensorFName = "AI Sensor Friendly"; //required for wingman operation
        String outputName = "AI Display"; //optional
        bool displayToName = false; // change the output block's name
        bool displayToLCD = false; // if the output block is a display, show output there
        int GatlingDistance = 600; // distance at which it's OK to start firing guns
        int RocketDistance = 400; // distance at which it's OK to start firing rockets
        bool AllowRockets = true; // if disabled, only shoot with gatlings (distinct from above so that it can be made an order on/off quickly)


        // global vars
        Vector3D OriginPos = new Vector3D(0, 0, 0);
        Vector3D MyselfPos = new Vector3D(0, 0, 0);
        Vector3D TargetPos = new Vector3D(0, 0, 0);
        Vector3D LeaderPos = new Vector3D(0, 0, 0);
        Vector3D prevMyselfPos = new Vector3D(0, 0, 0);
        Vector3D prevTargetPos = new Vector3D(0, 0, 0);
        Vector3D prevLeaderPos = new Vector3D(0, 0, 0);
        Vector3D WPPos = new Vector3D(0, 0, 0);
        String WPName = "Waypoint";
        IMySensorBlock sensorE = null;
        IMySensorBlock sensorF = null;
        IMyRemoteControl remote = null;
        IMyTextPanel output = null; // display data/debug to either this block's name, or if it's a lcd, its text
        VRage.Game.ModAPI.Ingame.IMyEntity enemyentity = null;
        VRage.Game.ModAPI.Ingame.IMyEntity leaderentity = null;
        int ticks = 0; // counts up
        int orderticks = 0; // counts down
        Orders CurrentOrder = Orders.ATTACK_ENEMY;
        int distance = 0;
        double spd, grav;
        bool AllowGuns = true;
        int ScoutAheadDistDone = 100; // scouting ahead distance inbound leg done, back to order 2
        String SysMessage = "";
        List<IMyTerminalBlock> list = null; // used for finding blocks

        enum Orders
        {
            ATTACK_ENEMY = 0,
            STAY_PUT,
            FOLLOW_LEADER,
            GOTO_ORIGIN,
            BACK_TO_ORDER
        }

        void Main(String order)
        {

            //get remote control block
            if (remote == null)
            {
                list = new List<IMyTerminalBlock>();
                GridTerminalSystem.GetBlocksOfType<IMyRemoteControl>(list);
                GridTerminalSystem.SearchBlocksOfName(remoteName, list, null);
                if (list.Count == 0 || list[0] == null)
                {
                    remote = null;
                }
                else
                {
                    remote = list[0] as IMyRemoteControl;
                }
                if (remote == null)
                {
                    Print("ERROR: No usable remote found, aborting");
                    return;
                }
            }

            // get sensor block for detecting enemies
            if (sensorE == null)
            {
                list = new List<IMyTerminalBlock>();
                GridTerminalSystem.GetBlocksOfType<IMySensorBlock>(list);
                GridTerminalSystem.SearchBlocksOfName(sensorEName, list, null);
                if (list.Count == 0 || list[0] == null)
                {
                    sensorE = null;
                }
                else
                {
                    sensorE = list[0] as IMySensorBlock;
                }
                if (sensorE == null)
                {
                    Print("ERROR: No enemy sensor found, NPC operation only"); // we can still operate if we belong to a NPC faction 
                }
            }

            // get sensor block for detecting friendlies
            if (sensorF == null)
            {
                list = new List<IMyTerminalBlock>();
                GridTerminalSystem.GetBlocksOfType<IMySensorBlock>(list);
                GridTerminalSystem.SearchBlocksOfName(sensorFName, list, null);
                if (list.Count == 0 || list[0] == null)
                {
                    sensorF = null;
                }
                else
                {
                    sensorF = list[0] as IMySensorBlock;
                }
                if (sensorF == null)
                {
                    Print("WARNING: No friendly sensor found, running solo"); // we can still operate if we belong to a NPC faction 
                }
            }

            // get output block
            if (output == null)
            {
                list = new List<IMyTerminalBlock>();
                GridTerminalSystem.GetBlocksOfType<IMyTextPanel>(list);
                GridTerminalSystem.SearchBlocksOfName(outputName, list, null);
                if (list.Count == 0 || list[0] == null)
                {
                    output = null;
                }
                else
                {
                    output = list[0] as IMyTextPanel;
                }
                if (output == null)
                {
                    Print("WARNING: No usable output found"); // we can still operate if we belong to a NPC faction 
                }
            }



            // get wher I am once per tick to reduce overhead
            MyselfPos = remote.GetPosition();
            // get some telemetry off the remote while we are at it
            spd = remote.GetShipSpeed();
            grav = remote.GetNaturalGravity().Length();
            //record origin position
            OriginPos = ReadOrigin(MyselfPos);
            Print("Home is at: " + OriginPos.X + " " + OriginPos.Y + " " + OriginPos.Z);
            distance = -1;


            //get enemy coordinates
            TargetPos = prevTargetPos;
            bool foundenemy = remote.GetNearestPlayer(out TargetPos);
            if (foundenemy)
            {
                Print("Enemy found by NPC GNP function at " + TargetPos.X + " " + TargetPos.Y + " " + TargetPos.Z);
                CurrentOrder = Orders.ATTACK_ENEMY; // makes sense in this case
            }
            if (foundenemy == false)
            {
                if (sensorE.LastDetectedEntity != null)
                {
                    enemyentity = sensorE.LastDetectedEntity; // this is not instant!
                    TargetPos = enemyentity.GetPosition();
                    if (!iszero(TargetPos))
                    {
                        Print("Enemy found by current sensor scan at " + TargetPos.X + " " + TargetPos.Y + " " + TargetPos.Z);
                        foundenemy = true;
                    }
                    else
                    {
                        Print("Enemy sensor scan gave bad result");
                        enemyentity = null;
                        foundenemy = false;
                    }
                }
                else if (enemyentity != null)
                {
                    TargetPos = enemyentity.GetPosition();
                    if (!iszero(TargetPos))
                    {
                        Print("Enemy remembered from sensor scan at " + TargetPos.X + " " + TargetPos.Y + " " + TargetPos.Z);
                        distance = Convert.ToInt32(Vector3D.Distance(TargetPos, MyselfPos)); // used for going home
                        foundenemy = true;
                    }
                    else
                    {
                        Print("Cannot remember sensor scan");
                        enemyentity = null;
                        foundenemy = false;
                    }
                }
                else
                {
                    Print("Unable to find a valid target");
                }
            }


            // get leader coordinates
            LeaderPos = prevLeaderPos;
            bool foundleader = false;
            if (sensorF == null)
            {
                Print("Solo mode; cannot detect valid wingleader");
            }
            else
            {
                if (sensorF.LastDetectedEntity != null)
                {
                    leaderentity = sensorF.LastDetectedEntity; // this is not instant!
                    LeaderPos = leaderentity.GetPosition();
                    if (!iszero(LeaderPos))
                    {
                        Print("Leader found by current sensor scan at " + LeaderPos.X + " " + LeaderPos.Y + " " + LeaderPos.Z);
                        sensorF.ApplyAction("OnOff_Off");
                        foundleader = true;
                    }
                    else
                    {
                        Print("Leaderensor scan gave bad result");
                        sensorF.ApplyAction("OnOff_On");
                        leaderentity = null;
                        foundleader = false;
                    }
                }
                else if (leaderentity != null)
                {
                    LeaderPos = leaderentity.GetPosition();
                    if (!iszero(LeaderPos))
                    {
                        Print("Leader remembered from sensor scan at " + LeaderPos.X + " " + LeaderPos.Y + " " + LeaderPos.Z);
                        foundleader = true;
                        distance = Convert.ToInt32(Vector3D.Distance(LeaderPos, MyselfPos)); // used for regrouping
                        sensorF.ApplyAction("OnOff_Off");
                    }
                    else
                    {
                        Print("Cannot remember leader sensor scan");
                        leaderentity = null;
                        foundleader = false;
                        sensorF.ApplyAction("OnOff_On");
                    }
                }
                else
                {
                    Print("Unable to find a valid wingleader");
                    foundleader = false;
                    sensorF.ApplyAction("OnOff_On");
                }
            }



            if (order.StartsWith("0")) // find and attack target
            {
                CurrentOrder = Orders.ATTACK_ENEMY;
                orderticks = -1;
            }
            if (order.StartsWith("1")) // stay put
            {
                CurrentOrder = Orders.STAY_PUT;
                orderticks = StayPutTime;
            }
            if (order.StartsWith("2")) // follow me
            {
                CurrentOrder = Orders.FOLLOW_LEADER;
                orderticks = -1;
            }
            if (order.StartsWith("3")) // return home
            {
                CurrentOrder = Orders.GOTO_ORIGIN;
                orderticks = -1;
            }
            if (order.StartsWith("4")) // scout away from me
            {
                CurrentOrder = Orders.BACK_TO_ORDER;
                orderticks = ScoutAheadTime;
            }

            // TODO Read orders from commandline / communications / whatever

            Orders CurrentPlan = CurrentOrder;

            // change plan based on order and opportunity

            if (CurrentPlan == Orders.ATTACK_ENEMY && foundenemy == false)
            {
                CurrentPlan = Orders.FOLLOW_LEADER;
                Print("No enemy, following wingleader");
            }

            if (CurrentPlan == Orders.FOLLOW_LEADER && foundleader == false)
            {
                CurrentPlan = Orders.GOTO_ORIGIN;
                Print("No wingleader, going home");
            }

            if (foundleader == true && distance > RejoinLeaderDistance)
            {
                CurrentPlan = Orders.FOLLOW_LEADER;
                CurrentOrder = Orders.FOLLOW_LEADER;
                Print("Very far from wingleader, regrouping");
            }

            if (foundenemy == true && foundleader == false && distance > GoHomeDistance)
            {
                CurrentPlan = Orders.GOTO_ORIGIN;
                Print("Very far from target, no wingleader, going home instead");
            }

            if (false && CurrentPlan == Orders.BACK_TO_ORDER && foundenemy == true)
            {
                CurrentPlan = Orders.ATTACK_ENEMY;
                Print("Found enemy, engaging!");
            }

            if (order.Length > 0)
            {
                Print("Received order " + CurrentOrder + " lasting " + orderticks + " ticks. Plan is " + CurrentPlan);
                order = "";
            }


            // execute plan

            // safety
            if (CurrentPlan > Orders.ATTACK_ENEMY)
                TurnFwdGunsOff();

            if (CurrentPlan == Orders.ATTACK_ENEMY)
            {
                bool kamikaze = (SafeDistanceEnemies == 0);
                distance = Convert.ToInt32(Vector3D.Distance(TargetPos, MyselfPos));
                Print("Going after enemy target, distance: " + distance + ",ramming: " + kamikaze);
                if ((kamikaze == false) && ((distance < SafeDistanceEnemies) || (distance < (spd * 2))))
                {
                    remote.ApplyAction("CollisionAvoidance_On");
                }
                else
                {
                    remote.ApplyAction("CollisionAvoidance_Off");
                }

                bool EnemyInCrosshairs = CheckDirection(remote, prevTargetPos, MyselfPos, distance);

                // see if there are any working forward facing weapons and if we are targeting an enemy and if yes, run em
                bool FwdGunsWork = kamikaze; // kamikaze basically forces this true, and causes a ram if there are no working guns
                int weapcount = 0;
                list.Clear();
                GridTerminalSystem.GetBlocksOfType<IMyUserControllableGun>(list);
                for (int i = 0; i < list.Count; i++)
                {
                    FwdGunsWork = FwdGunsWork || CheckWeaponsIsWorking(list[i] as IMyUserControllableGun); // if at least one gun is good, use it
                    weapcount += FixedWeaponControl(list[i] as IMyUserControllableGun, distance, EnemyInCrosshairs);
                }
                if (FwdGunsWork == false)
                {
                    Print("No working weapons or ramming option, too damaged to continue, going home");
                    CurrentOrder = Orders.GOTO_ORIGIN;
                    CurrentPlan = Orders.GOTO_ORIGIN;
                }
                else
                {
                    Print("Attacking target, current firepower output: " + weapcount);
                    WPPos = TargetPos;
                    WPName = "Enemy target, FPO:" + weapcount;
                }
            }
            if (CurrentPlan == Orders.STAY_PUT)
            {
                if (orderticks > 0)
                {
                    Print("Staying put for " + orderticks + " ticks");
                    WPPos = MyselfPos;
                    WPName = "Stationkeeping";
                    remote.ApplyAction("CollisionAvoidance_On");
                }
                else
                {
                    distance = Convert.ToInt32(Vector3D.Distance(LeaderPos, MyselfPos));
                    Print("Returning to wingleader");
                    WPPos = Midpoint(LeaderPos, MyselfPos);
                    WPName = "Scouting Vector In";
                    if (distance < ScoutAheadDistDone)
                        CurrentOrder = DefaultOrder;
                }
            }
            if (CurrentPlan == Orders.FOLLOW_LEADER)
            {
                distance = Convert.ToInt32(Vector3D.Distance(LeaderPos, MyselfPos));
                if ((distance < SafeDistanceLeader) || (distance < (spd * 3)))
                {
                    remote.ApplyAction("CollisionAvoidance_On");
                }
                else
                {
                    remote.ApplyAction("CollisionAvoidance_Off");
                }

                if (distance < (MinDistanceLeader * (1 + (spd / 20)))) // active braking
                {
                    Print("Following wingleader, but not too close");
                    WPPos = Farpoint(LeaderPos, MyselfPos);//MyselfPos;//DistalThird(prevMyselfPos,LeaderPos);
                    WPName = "Wingleader - Safe Distance";
                }
                else
                {
                    Print("Following wingleader");
                    WPPos = LeaderPos;
                    WPName = "Wingleader";
                }
            }
            if (CurrentPlan == Orders.GOTO_ORIGIN)
            {
                Print("Heading home");
                distance = Convert.ToInt32(Vector3D.Distance(OriginPos, MyselfPos));
                if (distance < SafeDistanceOrigin || (distance < (spd * 3)))
                {
                    remote.ApplyAction("CollisionAvoidance_On");
                }
                else
                {
                    remote.ApplyAction("CollisionAvoidance_Off");
                }
                WPPos = OriginPos;
                WPName = "Home";
            }
            if (CurrentPlan == Orders.BACK_TO_ORDER)
            {

                distance = Convert.ToInt32(Vector3D.Distance(LeaderPos, MyselfPos));
                if ((distance < SafeDistanceLeader) || (distance < (spd * 3)))
                {
                    remote.ApplyAction("CollisionAvoidance_On");
                }
                else
                {
                    remote.ApplyAction("CollisionAvoidance_Off");
                }

                if (orderticks > 0)
                {
                    Print("Scouting away from wingleader for " + orderticks + " ticks");
                    WPPos = Farpoint(LeaderPos, MyselfPos);
                    WPName = "Scouting Vector Out";
                }
                else
                {
                    Print("Returning to wingleader");
                    WPPos = Midpoint(LeaderPos, MyselfPos);
                    WPName = "Scouting Vector In";
                    if (distance < ScoutAheadDistDone)
                        CurrentOrder = DefaultOrder;
                }


            }
            if (CurrentPlan > Orders.BACK_TO_ORDER)
            {
                Print("Unknown targeting state " + (int)CurrentPlan);
            }


            remote.ClearWaypoints();
            remote.AddWaypoint(WPPos, WPName);
            remote.SetAutoPilotEnabled(true);

            prevLeaderPos = LeaderPos;
            prevTargetPos = TargetPos;
            prevMyselfPos = MyselfPos;
            FinalizePrint();
            ticks++;
            if (orderticks > -1)
                orderticks--;
        }


        Vector3D vm = new Vector3D(0, 0, 0);
        Vector3D vm2 = new Vector3D(0, 0, 0);
        Vector3D Midpoint(Vector3D v1, Vector3D v2) // v1---mp---v2
        {
            vm.X = (v1.X + v2.X) / 2.0;
            vm.Y = (v1.Y + v2.Y) / 2.0;
            vm.Z = (v1.Z + v2.Z) / 2.0;
            return vm;
        }

        Vector3D DistalThird(Vector3D v1, Vector3D v2) // v1----dt--v2
        {
            vm.X = (v1.X + v2.X + v2.X) / 3.0;
            vm.Y = (v1.Y + v2.Y + v2.Y) / 3.0;
            vm.Z = (v1.Z + v2.Z + v2.Z) / 3.0;
            return vm;
        }

        Vector3D ProximalThird(Vector3D v1, Vector3D v2) // v1--pt----v2
        {
            vm.X = (v1.X + v1.X + v2.X) / 3.0;
            vm.Y = (v1.Y + v1.Y + v2.Y) / 3.0;
            vm.Z = (v1.Z + v1.Z + v2.Z) / 3.0;
            return vm;
        }

        Vector3D Farpoint(Vector3D v1, Vector3D v2) // v1------v2------fp
        {
            vm.X = (v1.X + v2.X) / 2.0;
            vm.Y = (v1.Y + v2.Y) / 2.0;
            vm.Z = (v1.Z + v2.Z) / 2.0;
            vm2.X = v2.X + (vm.X - v1.X);
            vm2.Y = v2.Y + (vm.Y - v1.Y);
            vm2.Z = v2.Z + (vm.Z - v1.Z);
            return vm2;
        }

        void Print(string what)
        {
            Echo(what);
            SysMessage = SysMessage + what + "\n";
        }

        void FinalizePrint()
        {
            if (output == null) return;
            output.WritePublicText(SysMessage);
            //Echo (SysMessage);
            SysMessage = "";
        }

        bool iszero(Vector3D vec)
        {
            return ((vec.X == 0.0) && (vec.Y == 0.0) && (vec.Z == 0.0));
        }
        void TurnFwdGunsOff()
        {
            list.Clear();
            GridTerminalSystem.GetBlocksOfType<IMyUserControllableGun>(list);
            for (int i = 0; i < list.Count; i++)
            {
                list[i].ApplyAction("Shoot_Off");
            }

        }


        Vector3D ReadOrigin(Vector3D myPos)
        {
            Vector3D origin = new Vector3D(0, 0, 0);
            origin = OriginPos;
            if (this.Storage == null || this.Storage == "" || this.Storage == "(X:0 Y:0 Z:0)")
            {
                origin = myPos;
                OriginPos = origin;
                this.Storage = origin.ToString();
            }
            else if (OriginPos.X == 0 && OriginPos.Y == 0 && OriginPos.Z == 0)
            {
                Vector3D.TryParse(this.Storage, out origin);
                OriginPos = origin;
            }
            else
            {
                origin = OriginPos;
            }
            return origin;
        }

        bool CheckWeaponsIsWorking(IMyUserControllableGun gun)
        {
            if (!gun.IsFunctional || !gun.IsWorking)
            { return false; }
            if (gun.HasInventory() && !gun.GetInventory(0).IsItemAt(0))
            { return false; }
            return true;
        }


        bool CheckDirection(IMyRemoteControl remote, Vector3D TargetPos, Vector3D myPos, int distanceFromTarget)
        {
            //get forward
            var droneForward = remote.Position + Base6Directions.GetIntVector(remote.Orientation.TransformDirection(Base6Directions.Direction.Forward));
            var droneForwardVector = remote.CubeGrid.GridIntegerToWorld(droneForward);
            var droneForwardVectorNormalize = Vector3D.Normalize(droneForwardVector - myPos);
            //get drone to target
            Vector3D droneToTargetVector = TargetPos - myPos;
            //set length
            Vector3D droneForwardVectorToTarget = droneForwardVectorNormalize * distanceFromTarget;
            double angleRadius = Vector3D.Distance(droneToTargetVector, droneForwardVectorToTarget);
            //compare
            if (angleRadius < GoodEnoughAngle)
            { return true; }
            return false;
        }

        int FixedWeaponControl(IMyUserControllableGun currentWeapon, int distanceFromTarget, bool applyFire)
        {
            if (currentWeapon is IMySmallGatlingGun)
            {
                if (distanceFromTarget < GatlingDistance && applyFire && AllowGuns)
                { currentWeapon.ApplyAction("Shoot_On"); return 1; }
                else
                { currentWeapon.ApplyAction("Shoot_Off"); return 0; }
            }
            if (currentWeapon is IMySmallMissileLauncher || currentWeapon is IMySmallMissileLauncherReload)
            {
                if (distanceFromTarget < RocketDistance && applyFire && AllowRockets)
                { currentWeapon.ApplyAction("Shoot_On"); return 100; }
                else
                { currentWeapon.ApplyAction("Shoot_Off"); return 0; }
            }
            return -10000;
        }


        //Simple TokenAt function 
        string TokenAt(string Text, int Index, char Delimiter = ':') { string[] TokenList = Text.Split(Delimiter); return TokenList[Index]; }

        //Parses float from string 
        float Parse(string input) { return float.Parse(input, System.Globalization.CultureInfo.InvariantCulture); }

        //Parses a Vector3D from three float inputs 
        Vector3D CoordParse(float X, float Y, float Z) { Vector3D output = new Vector3D(X, Y, Z); return output; }

        //Random function 
        int Rand(int min = 0, int max = 100, int randomizer = 1) { Random random = new Random(randomizer); int n = random.Next(min, (int)max); return n; }

        void PrintMsg(object obj)
        {
            if (displayToName == false && displayToLCD == false)
                return;
            if (obj == null)
            {
                Print("ERROR: message to print was NULL");
                return;
            }
            if (displayToName == true)
                output.SetCustomName(outputName + " - " + obj);
        }
    }
}