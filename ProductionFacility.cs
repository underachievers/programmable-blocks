﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;

using VRageMath;
using VRage.Game;
using VRage.Collections;
using Sandbox.ModAPI.Ingame;
using VRage.Game.Components;
using VRage.Game.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using Sandbox.Game.EntityComponents;
using SpaceEngineers.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;

namespace SpaceEngineers.ProductionFacility
{
    public sealed class Program : MyGridProgram
    {
        //=======================================================================
        //////////////////////////BEGIN//////////////////////////////////////////
        //=======================================================================
        string FACTORY_GROUP_NAME = "FactoryTry";

        enum FactoryBlockType
        {
            // 
            // Small ship components. 
            // 
            SSLightArmorBlock,
            SSLightArmorSlope,
            SSLightArmorCorner,
            SSLightArmorInvCorner,
            SSHeavyArmorBlock,
            SSHeavyArmorSlope,
            SSHeavyArmorCorner,
            SSHeavyArmorInvCorner,
            SSRoundArmorSlope,
            SSRoundArmorCorner,
            SSRoundArmorInvCorner,
            SSHeavyArmorRoundSlope,
            SSHeavyArmorRoundCorner,
            SSHeavyArmorRoundInvCorner,
            SSLightArmorSlope2x1x1Base,
            SSLightArmorSlope2x1x1Tip,
            SSHeavyArmorSlope2x1x1Base,
            SSHeavyArmorSlope2x1x1Tip,
            SSLightArmorCorner2x1x1Base,
            SSLightArmorCorner2x1x1Tip,
            SSHeavyArmorCorner2x1x1Base,
            SSHeavyArmorCorner2x1x1Tip,
            SSLightArmorInvCorner2x1x1Base,
            SSLightArmorInvCorner2x1x1Tip,
            SSHeavyArmorInvCorner2x1x1Base,
            SSHeavyArmorInvCorner2x1x1Tip,
            SSCockpit,
            SSFighterCockpit,
            SSRemoteControl,
            SSPassengetSeat,
            SSBattery,
            SSSolarPanel,
            SSSmallReactor,
            SSLargeReactor,
            SSHydrogenThruster,
            SSLargeHydrogenThruster,
            SSAtmosphericThruster,
            SSLargeAtmosphericThruster,
            SSIonjThruster,
            SSLargeIonThruster,
            SSWheels,
            SSWheels5x5,
            SSWheels1x1,
            SSWheelsSuspension,
            SSWheelsSuspension1x1,
            SSWheelsSuspension5x5,
            SSGyroscope,
            SSOreDetector,
            SSAntenna,
            SSBeacon,
            SSLaserAntenna,
            SSProjector,
            SSSmallCargoContainer,
            SSMediumCargoContainer,
            SSLarceCargoContainer,
            SSGatlingTurret,
            SSMissileTurret,
            SSGatlingGun,
            SSRocketLauncher,
            SSReloadableRocketLauncher,
            SSWarhead,
            SSWelder,
            SSGrinder,
            SSDrill,
            SSBlastDoor,
            SSBlastDoorCorner,
            SSBlastDoorCornerInv,
            SSBlastDoorEdge,
            SSDecoy,
            SSInteriorLight,
            SSSpotlight,
            SSProgrammableBlock,
            SSControlPanel,
            SSCamera,
            SSSoundBlock,
            SSSensor,
            SSTimerBlock,
            SSButtonPanel,
            SSLCDPanel,
            SSWideLCDPanel,
            SSTextPanel,
            SSConveyorTube,
            SSConveyorJunction,
            SSCurvedConveyprTube,
            SSConnector,
            SSEjector,
            SSCollector,
            SSConveyorSorter,
            SSLandingGear,
            SSPiston,
            SSAdvancedRotor,
            SSMergeBlock,
            SSRotorPart,
            SSAdvancedRotorPart,
            SSAirVent,
            SSOxygenGenerator,
            SSOxygenTank,
            SSHydrogenTank,
            SSArtificialMass,
            SSSpaceBall,

            // 
            // Large ship components. 
            // 
            LSLightArmorBlock,
            LSLightArmorSlope,
            LSLightArmorCorner,
            LSLightArmorInvCorner,
            LSHeavyArmorBlock,
            LSHeavyArmorSlope,
            LSHeavyArmorCorner,
            LSHeavyArmorInvCorner,
            LSInteriorWall,
            LSRoundArmorSlope,
            LSRoundArmorCorner,
            LSRoundArmorInvCorner,
            LSHeavyArmorRoundSlope,
            LSHeavyArmorRoundCorner,
            LSHeavyArmorRoundInvCorner,
            LSLightArmorSlope2x1x1Base,
            LSLightArmorSlope2x1x1Tip,
            LSHeavyArmorSlope2x1x1Base,
            LSHeavyArmorSlope2x1x1Tip,
            LSLightArmorCorner2x1x1Base,
            LSLightArmorCorner2x1x1Tip,
            LSHeavyArmorCorner2x1x1Base,
            LSHeavyArmorCorner2x1x1Tip,
            LSLightArmorInvCorner2x1x1Base,
            LSLightArmorInvCorner2x1x1Tip,
            LSHeavyArmorInvCorner2x1x1Base,
            LSHeavyArmorInvCorner2x1x1Tip,
            LSControlStation,
            LSFlightSeat,
            LSCockpit,
            LSRemoteControl,
            LSPassengetSeat,
            LSBattery,
            LSSolarPanel,
            LSSmallReactor,
            LSLargeReactor,
            LSMedicalRoom,
            LSCryoChamber,
            LSHydrogenThruster,
            LSLargeHydrogenThruster,
            LSAtmosphericThruster,
            LSLargeAtmosphericThruster,
            LSIonjThruster,
            LSLargeIonThruster,
            LSWheels,
            LSWheels5x5,
            LSWheels1x1,
            LSWheelsSuspension,
            LSWheelsSuspension1x1,
            LSWheelsSuspension5x5,
            LSGyroscope,
            LSOreDetector,
            LSAntenna,
            LSBeacon,
            LSLaserAntenna,
            LSRefinery,
            LSAssembler,
            LSProductivityModule,
            LSEffectivenessModule,
            LSPowerEfficiencyModule,
            LSArcFurnace,
            LSProjector,
            LSSmallCargoContainer,
            LSLarceCargoContainer,
            LSInteriorTurret,
            LSGatlingTurret,
            LSMissileTurret,
            LSRocketLauncher,
            LSWarhead,
            LSWelder,
            LSGrinder,
            LSDrill,
            LSSlidingDoor,
            LSDoor,
            LSAirtightHangarDoor,
            LSBlastDoor,
            LSBlastDoorCorner,
            LSBlastDoorCornerInv,
            LSBlastDoorEdge,
            LSCoverWall,
            LSHalfCoverWall,
            LSSteelCatwalk,
            LSSteelCatwalkTwoSides,
            LSSteelCatwalkCorner,
            LSSteelCatwalkPlate,
            LSStairs,
            LSInteriorPillar,
            LSVerticalWindow,
            LSDiagonalWindow,
            LSPassage,
            LSDecoy,
            LSInteriorLight,
            LSSpotlight,
            LSProgrammableBlock,
            LSControlPanel,
            LSCamera,
            LSSoundBlock,
            LSSensor,
            LSTimerBlock,
            LSButtonPanel,
            LSLCDPanel,
            LSWideLCDPanel,
            LSTextPanel,
            LSConveyorTube,
            LSConveyorJunction,
            LSCurvedConveyprTube,
            LSConnector,
            LSCollector,
            LSConveyorSorter,
            LSLandingGear,
            LSPiston,
            LSAdvancedRotor,
            LSMergeBlock,
            LSRotorPart,
            LSAdvancedRotorPart,
            LSAirVent,
            LSOxygenGenerator,
            LSOxygenTank,
            LSHydrogenTank,
            LSOxygenFarm,
            LSWindow1x1Flat,
            LSWindow1x1FlatInv,
            LSWindow1x1Slope,
            LSWindow1x2Flat,
            LSWindow1x2FlatInv,
            LSWindow1x2Slope,
            LSWindow1x2SideLeft,
            LSWindow1x2SideRight,
            LSWindow1x1Face,
            LSWindow1x1Side,
            LSWindow1x1Inv,
            LSWindow1x2Face,
            LSWindow1x2Inv,
            LSWindow2x3Flat,
            LSWindow2x3FlatInv,
            LSWindow3x3Flat,
            LSWindow3x3FlatInv,
            LSGravityGenerator,
            LSSphericalGravityGenerator,
            LSArtificialMass,
            LSSpaceBall,
            LSJumpDrive
        };

        enum MainMenu
        {
            Main = 0,

            SSMain,
            SSArmor,
            SSLightArmor,
            SSHeavyArmor,
            SSCockpit,
            SSPower,
            SSThrusters,
            SSWheels,
            SSInternals,
            SSCargo,
            SSWeapons,
            SSTools,
            SSBlastDoors,
            SSLightsDecoy,
            SSFunctional,
            SSDisplays,
            SSConveyors,
            SSMechanical,

            LSMain,
            LSArmor,
            LSLightArmor,
            LSHeavyArmor,
            LSCockpit,
            LSPower,
            LSMedical,
            LSThrusters,
            LSWheels,
            LSInternals,
            LSRefining,
            LSProjector,
            LSCargo,
            LSWeapons,
            LSTools,
            LSDoors,
            LSInterior,
            LSLightsDecoy,
            LSFunctional,
            LSDisplays,
            LSConveyors,
            LSMechanical,
            LSWindows,
            LSQuantum,

            Invalid
        };

        private class FactoryComponents
        {
            public int bulletproof_glass;
            public int computers;
            public int construction_comp;
            public int detector_comp;
            public int display;
            public int explosives;
            public int girder;
            public int grav_gen_comp;
            public int interior_plate;
            public int large_steel_tube;
            public int medical_comp;
            public int metal_grid;
            public int missile_200mm;
            public int motor;
            public int nato_25x184mm;
            public int nato_556x45mm;
            public int power_cell;
            public int radio_comm_comp;
            public int reactor_comp;
            public int small_stell_tube;
            public int solar_cell;
            public int steel_plate;
            public int superconductor_con;
            public int thruster_comp;

            public FactoryComponents()
            {
                bulletproof_glass = 0;
                computers = 0;
                construction_comp = 0;
                detector_comp = 0;
                display = 0;
                explosives = 0;
                girder = 0;
                grav_gen_comp = 0;
                interior_plate = 0;
                large_steel_tube = 0;
                medical_comp = 0;
                metal_grid = 0;
                missile_200mm = 0;
                motor = 0;
                nato_25x184mm = 0;
                nato_556x45mm = 0;
                power_cell = 0;
                radio_comm_comp = 0;
                reactor_comp = 0;
                small_stell_tube = 0;
                solar_cell = 0;
                steel_plate = 0;
                superconductor_con = 0;
                thruster_comp = 0;
            }

            public void Add(FactoryComponents fc, int mul = 1)
            {
                bulletproof_glass += fc.bulletproof_glass * mul;
                computers += fc.computers * mul;
                construction_comp += fc.construction_comp * mul;
                detector_comp += fc.detector_comp * mul;
                display += fc.display * mul;
                explosives += fc.explosives * mul;
                girder += fc.girder * mul;
                grav_gen_comp += fc.grav_gen_comp * mul;
                interior_plate += fc.interior_plate * mul;
                large_steel_tube += fc.large_steel_tube * mul;
                medical_comp += fc.medical_comp * mul;
                metal_grid += fc.metal_grid * mul;
                missile_200mm += fc.missile_200mm * mul;
                motor += fc.motor * mul;
                nato_25x184mm += fc.nato_25x184mm * mul;
                nato_556x45mm += fc.nato_556x45mm * mul;
                power_cell += fc.power_cell * mul;
                radio_comm_comp += fc.radio_comm_comp * mul;
                reactor_comp += fc.reactor_comp * mul;
                small_stell_tube += fc.small_stell_tube * mul;
                solar_cell += fc.solar_cell * mul;
                steel_plate += fc.steel_plate * mul;
                superconductor_con += superconductor_con * mul;
                thruster_comp += fc.thruster_comp * mul;
            }
        }

        private class MenuFlow
        {
            public Dictionary<int, MainMenu> states;

            public MenuFlow()
            {
                states = new Dictionary<int, MainMenu>();
            }

            public MainMenu GetState(int s)
            {
                if (s < states.Count)
                    return states[s];

                return MainMenu.Invalid;
            }
        }

        Dictionary<MainMenu, MenuFlow> MainMenuFlow = new Dictionary<MainMenu, MenuFlow>();

        int quant_ones;
        int quant_tens;
        int quant_hundreds;
        int quant_thousands;

        int[] main_menu_prev = new int[4];
        MainMenu main_menu_state;
        int main_menu_level;
        int main_menu_pos;
        int main_menu_max;

        Dictionary<string, IMyTerminalBlock> FactoryBlocks = new Dictionary<string, IMyTerminalBlock>();

        private void LoadFactory()
        {
            IMyBlockGroup FactoryGroup = GridTerminalSystem.GetBlockGroupWithName(FACTORY_GROUP_NAME);
            var blocks = new List<IMyTerminalBlock>();

            FactoryGroup.GetBlocks(blocks);

            for (int loop = 0, max = blocks.Count; loop < max; ++loop)
            {
                FactoryBlocks[blocks[loop].CustomName] = blocks[loop];
            }
        }

        private IMyTerminalBlock GetControl(string ctrlid)
        {
            return FactoryBlocks[ctrlid];
        }

        public Program()
        {
            LoadFactory();

            var main = GetControl("LCD Panel Menu") as IMyTextPanel;

            main.ShowTextureOnScreen();
            main.ShowPublicTextOnScreen();

            var quant = GetControl("LCD Panel Quantity") as IMyTextPanel;

            quant.ShowTextureOnScreen();
            quant.ShowPublicTextOnScreen();

            var ctrl = GetControl("LCD Panel Control") as IMyTextPanel;

            ctrl.ShowTextureOnScreen();
            ctrl.ShowPublicTextOnScreen();

            quant_ones = 0;
            quant_tens = 0;
            quant_hundreds = 0;
            quant_thousands = 0;

            main_menu_pos = 0;
            main_menu_max = 0;
            main_menu_level = -1;
            main_menu_state = MainMenu.Main;

            // 
            // Initialize the main menu. 
            // 
            MainMenuFlow[MainMenu.Main] = new MenuFlow();

            MainMenuFlow[MainMenu.SSMain] = new MenuFlow();
            MainMenuFlow[MainMenu.SSArmor] = new MenuFlow();
            MainMenuFlow[MainMenu.SSLightArmor] = new MenuFlow();
            MainMenuFlow[MainMenu.SSHeavyArmor] = new MenuFlow();
            MainMenuFlow[MainMenu.SSCockpit] = new MenuFlow();
            MainMenuFlow[MainMenu.SSPower] = new MenuFlow();
            MainMenuFlow[MainMenu.SSThrusters] = new MenuFlow();
            MainMenuFlow[MainMenu.SSWheels] = new MenuFlow();
            MainMenuFlow[MainMenu.SSInternals] = new MenuFlow();
            MainMenuFlow[MainMenu.SSCargo] = new MenuFlow();
            MainMenuFlow[MainMenu.SSWeapons] = new MenuFlow();
            MainMenuFlow[MainMenu.SSTools] = new MenuFlow();
            MainMenuFlow[MainMenu.SSBlastDoors] = new MenuFlow();
            MainMenuFlow[MainMenu.SSLightsDecoy] = new MenuFlow();
            MainMenuFlow[MainMenu.SSFunctional] = new MenuFlow();
            MainMenuFlow[MainMenu.SSDisplays] = new MenuFlow();
            MainMenuFlow[MainMenu.SSConveyors] = new MenuFlow();
            MainMenuFlow[MainMenu.SSMechanical] = new MenuFlow();

            MainMenuFlow[MainMenu.LSMain] = new MenuFlow();
            MainMenuFlow[MainMenu.LSArmor] = new MenuFlow();
            MainMenuFlow[MainMenu.LSLightArmor] = new MenuFlow();
            MainMenuFlow[MainMenu.LSHeavyArmor] = new MenuFlow();
            MainMenuFlow[MainMenu.LSCockpit] = new MenuFlow();
            MainMenuFlow[MainMenu.LSPower] = new MenuFlow();
            MainMenuFlow[MainMenu.LSMedical] = new MenuFlow();
            MainMenuFlow[MainMenu.LSThrusters] = new MenuFlow();
            MainMenuFlow[MainMenu.LSWheels] = new MenuFlow();
            MainMenuFlow[MainMenu.LSInternals] = new MenuFlow();
            MainMenuFlow[MainMenu.LSRefining] = new MenuFlow();
            MainMenuFlow[MainMenu.LSProjector] = new MenuFlow();
            MainMenuFlow[MainMenu.LSCargo] = new MenuFlow();
            MainMenuFlow[MainMenu.LSWeapons] = new MenuFlow();
            MainMenuFlow[MainMenu.LSTools] = new MenuFlow();
            MainMenuFlow[MainMenu.LSDoors] = new MenuFlow();
            MainMenuFlow[MainMenu.LSInterior] = new MenuFlow();
            MainMenuFlow[MainMenu.LSLightsDecoy] = new MenuFlow();
            MainMenuFlow[MainMenu.LSFunctional] = new MenuFlow();
            MainMenuFlow[MainMenu.LSDisplays] = new MenuFlow();
            MainMenuFlow[MainMenu.LSConveyors] = new MenuFlow();
            MainMenuFlow[MainMenu.LSMechanical] = new MenuFlow();
            MainMenuFlow[MainMenu.LSWindows] = new MenuFlow();
            MainMenuFlow[MainMenu.LSQuantum] = new MenuFlow();

            var m = MainMenuFlow[MainMenu.Main];
            m.states[-1] = MainMenu.Main;
            m.states[0] = MainMenu.SSMain;
            m.states[1] = MainMenu.LSMain;

            m = MainMenuFlow[MainMenu.SSMain];
            m.states[-1] = MainMenu.Main;
            m.states[0] = MainMenu.SSArmor;
            m.states[1] = MainMenu.SSCockpit;
            m.states[2] = MainMenu.SSPower;
            m.states[3] = MainMenu.SSThrusters;
            m.states[4] = MainMenu.SSWheels;
            m.states[5] = MainMenu.SSInternals;
            m.states[6] = MainMenu.SSCargo;
            m.states[7] = MainMenu.SSWeapons;
            m.states[8] = MainMenu.SSTools;
            m.states[9] = MainMenu.SSBlastDoors;
            m.states[10] = MainMenu.SSLightsDecoy;
            m.states[11] = MainMenu.SSFunctional;
            m.states[12] = MainMenu.SSDisplays;
            m.states[13] = MainMenu.SSConveyors;
            m.states[14] = MainMenu.SSMechanical;

            m = MainMenuFlow[MainMenu.SSArmor];
            m.states[-1] = MainMenu.SSMain;
            m.states[0] = MainMenu.SSLightArmor;
            m.states[1] = MainMenu.SSHeavyArmor;

            m = MainMenuFlow[MainMenu.SSLightArmor];
            m.states[-1] = MainMenu.SSArmor;

            m = MainMenuFlow[MainMenu.SSHeavyArmor];
            m.states[-1] = MainMenu.SSArmor;

            m = MainMenuFlow[MainMenu.SSCockpit];
            m.states[-1] = MainMenu.SSMain;

            m = MainMenuFlow[MainMenu.SSPower];
            m.states[-1] = MainMenu.SSMain;

            m = MainMenuFlow[MainMenu.SSThrusters];
            m.states[-1] = MainMenu.SSMain;

            m = MainMenuFlow[MainMenu.SSWheels];
            m.states[-1] = MainMenu.SSMain;

            m = MainMenuFlow[MainMenu.SSInternals];
            m.states[-1] = MainMenu.SSMain;

            m = MainMenuFlow[MainMenu.SSCargo];
            m.states[-1] = MainMenu.SSMain;

            m = MainMenuFlow[MainMenu.SSWeapons];
            m.states[-1] = MainMenu.SSMain;

            m = MainMenuFlow[MainMenu.SSTools];
            m.states[-1] = MainMenu.SSMain;

            m = MainMenuFlow[MainMenu.SSBlastDoors];
            m.states[-1] = MainMenu.SSMain;

            m = MainMenuFlow[MainMenu.SSLightsDecoy];
            m.states[-1] = MainMenu.SSMain;

            m = MainMenuFlow[MainMenu.SSFunctional];
            m.states[-1] = MainMenu.SSMain;

            m = MainMenuFlow[MainMenu.SSDisplays];
            m.states[-1] = MainMenu.SSMain;

            m = MainMenuFlow[MainMenu.SSConveyors];
            m.states[-1] = MainMenu.SSMain;

            m = MainMenuFlow[MainMenu.SSMechanical];
            m.states[-1] = MainMenu.SSMain;

            m = MainMenuFlow[MainMenu.LSMain];
            m.states[-1] = MainMenu.Main;
            m.states[0] = MainMenu.LSArmor;
            m.states[1] = MainMenu.LSCockpit;
            m.states[2] = MainMenu.LSPower;
            m.states[3] = MainMenu.LSMedical;
            m.states[4] = MainMenu.LSThrusters;
            m.states[5] = MainMenu.LSWheels;
            m.states[6] = MainMenu.LSInternals;
            m.states[7] = MainMenu.LSRefining;
            m.states[8] = MainMenu.LSProjector;
            m.states[9] = MainMenu.LSCargo;
            m.states[10] = MainMenu.LSWeapons;
            m.states[11] = MainMenu.LSTools;
            m.states[12] = MainMenu.LSDoors;
            m.states[13] = MainMenu.LSInterior;
            m.states[14] = MainMenu.LSLightsDecoy;
            m.states[15] = MainMenu.LSFunctional;
            m.states[16] = MainMenu.LSDisplays;
            m.states[17] = MainMenu.LSConveyors;
            m.states[18] = MainMenu.LSMechanical;
            m.states[19] = MainMenu.LSWindows;
            m.states[20] = MainMenu.LSQuantum;

            m = MainMenuFlow[MainMenu.LSArmor];
            m.states[-1] = MainMenu.LSMain;
            m.states[0] = MainMenu.LSLightArmor;
            m.states[1] = MainMenu.LSHeavyArmor;

            m = MainMenuFlow[MainMenu.LSLightArmor];
            m.states[-1] = MainMenu.LSArmor;

            m = MainMenuFlow[MainMenu.LSHeavyArmor];
            m.states[-1] = MainMenu.LSArmor;

            m = MainMenuFlow[MainMenu.LSCockpit];
            m.states[-1] = MainMenu.LSMain;

            m = MainMenuFlow[MainMenu.LSPower];
            m.states[-1] = MainMenu.LSMain;

            m = MainMenuFlow[MainMenu.LSMedical];
            m.states[-1] = MainMenu.LSMain;

            m = MainMenuFlow[MainMenu.LSThrusters];
            m.states[-1] = MainMenu.LSMain;

            m = MainMenuFlow[MainMenu.LSWheels];
            m.states[-1] = MainMenu.LSMain;

            m = MainMenuFlow[MainMenu.LSInternals];
            m.states[-1] = MainMenu.LSMain;

            m = MainMenuFlow[MainMenu.LSRefining];
            m.states[-1] = MainMenu.LSMain;

            m = MainMenuFlow[MainMenu.LSProjector];
            m.states[-1] = MainMenu.LSMain;

            m = MainMenuFlow[MainMenu.LSCargo];
            m.states[-1] = MainMenu.LSMain;

            m = MainMenuFlow[MainMenu.LSWeapons];
            m.states[-1] = MainMenu.LSMain;

            m = MainMenuFlow[MainMenu.LSTools];
            m.states[-1] = MainMenu.LSMain;

            m = MainMenuFlow[MainMenu.LSDoors];
            m.states[-1] = MainMenu.LSMain;

            m = MainMenuFlow[MainMenu.LSInterior];
            m.states[-1] = MainMenu.LSMain;

            m = MainMenuFlow[MainMenu.LSLightsDecoy];
            m.states[-1] = MainMenu.LSMain;

            m = MainMenuFlow[MainMenu.LSFunctional];
            m.states[-1] = MainMenu.LSMain;

            m = MainMenuFlow[MainMenu.LSDisplays];
            m.states[-1] = MainMenu.LSMain;

            m = MainMenuFlow[MainMenu.LSConveyors];
            m.states[-1] = MainMenu.LSMain;

            m = MainMenuFlow[MainMenu.LSMechanical];
            m.states[-1] = MainMenu.LSMain;

            m = MainMenuFlow[MainMenu.LSWindows];
            m.states[-1] = MainMenu.LSMain;

            m = MainMenuFlow[MainMenu.LSQuantum];
            m.states[-1] = MainMenu.LSMain;

            UpdateMainMenu();
            UpdateVolDisp();
        }

        public void Save()
        {

            // Called when the program needs to save its state. Use  
            // this method to save your state to the Storage field  
            // or some other means.   
            //   
            // This method is optional and can be removed if not  
            // needed.  
        }

        public void Main(string argument)
        {
            if (argument.StartsWith("button_menu") == true)
                MenuButtons(argument);
            else if (argument.StartsWith("button_quant") == true)
                QuantButtons(argument);
            else if (argument.StartsWith("button_control") == true)
                ControlButtons(argument);
            else if (argument == "timer_1sec")
                DoTimer();
        }

        private void DoTimer()
        {
        }

        private void ControlButtons(string argument)
        {
        }

        private void MenuButtons(string argument)
        {
            if (argument == "button_menu_1")
            {
                if (--main_menu_pos < 0)
                    main_menu_pos = main_menu_max - 1;
            }
            else if (argument == "button_menu_2")
            {
                if (++main_menu_pos >= main_menu_max)
                    main_menu_pos = 0;
            }
            else if (argument == "button_menu_3")
                MainMenuSelect();
            else if (argument == "button_menu_4")
                MainMenuSelect(true);

            UpdateMainMenu();
        }

        private void MainMenuSelect(bool back = false)
        {
            if (MainMenuFlow[main_menu_state] == null)
                return;

            if ((MainMenuFlow[main_menu_state].states.Count == 1) && (back == false))
                return;

            if (back == true)
                main_menu_pos = -1;

            main_menu_state = MainMenuFlow[main_menu_state].states[main_menu_pos];

            if (back == false)
            {
                main_menu_prev[++main_menu_level] = main_menu_pos;
                main_menu_pos = 0;
            }
            else
            {
                if (main_menu_level > -1)
                    main_menu_pos = main_menu_prev[main_menu_level--];
                else
                    main_menu_pos = 0;
            }
        }

        private void ResetQuant()
        {
            quant_ones = 0;
            quant_tens = 0;
            quant_hundreds = 0;
            quant_thousands = 0;

            UpdateVolDisp();
        }

        private void UpdateMainMenu()
        {
            var str = new List<string>();

            switch (main_menu_state)
            {
                case MainMenu.Main:
                    {
                        str.Add("    Small Ship");
                        str.Add("    Large Ship");
                        break;
                    }

                case MainMenu.SSMain:
                    {
                        str.Add("    Armor");
                        str.Add("    Cockpits");
                        str.Add("    Power");
                        str.Add("    Thrusters");
                        str.Add("    Wheels");
                        str.Add("    Internals/Projector");
                        str.Add("    Cargo Containers");
                        str.Add("    Weapons");
                        str.Add("    Tools");
                        str.Add("    Blast Doora");
                        str.Add("    Lights/Decoy");
                        str.Add("    Functional");
                        str.Add("    Displays");
                        str.Add("    Conveyors");
                        str.Add("    Mechanical");
                        break;
                    }

                case MainMenu.SSArmor:
                    {
                        str.Add("    Light Armor");
                        str.Add("    Heavy Armor");
                        break;
                    }

                case MainMenu.SSLightArmor:
                    {
                        str.Add("    Light Armor Block");
                        str.Add("    Light Armor Slope");
                        str.Add("    Light Armor Corner");
                        str.Add("    Light Armor Inv Corner");
                        str.Add("    Round Armor Slope");
                        str.Add("    Round Armor Corner");
                        str.Add("    Round Armor Inv Corner");
                        str.Add("    Light Armor Slope 2x1x1 Base");
                        str.Add("    Light Armor Slope 2x1x1 Tip");
                        str.Add("    Light Armor Corner 2x1x1 Base");
                        str.Add("    Light Armor Corner 2x1x1 Tip");
                        str.Add("    Light Armor Inv Corner 2x1x1 Base");
                        str.Add("    Light Armor Inv Corner 2x1x1 Tip");
                        break;
                    }

                case MainMenu.SSHeavyArmor:
                    {
                        str.Add("    Heavy Armor Block");
                        str.Add("    Heavy Armor Slope");
                        str.Add("    Heavy Armor Corner");
                        str.Add("    Heavy Armor Inv Corner");
                        str.Add("    Heavy Armor Round Slope");
                        str.Add("    Heavy Armor Round Corner");
                        str.Add("    Heavy Armor RoundInv Corner");
                        str.Add("    Heavy Armor Slope 2x1x1 Base");
                        str.Add("    Heavy Armor Slope 2x1x1 Tip");
                        str.Add("    Heavy Armor Corner 2x1x1 Base");
                        str.Add("    Heavy Armor Corner 2x1x1 Tip");
                        str.Add("    Heavy Armor Inv Corner 2x1x1 Base");
                        str.Add("    Heavy Armor Inv Corner 2x1x1 Tip");
                        break;
                    }

                case MainMenu.SSCockpit:
                    {
                        str.Add("    Cockpit");
                        str.Add("    Fighter Cockpit");
                        str.Add("    Remote Control");
                        str.Add("    Passenger Seat");
                        break;
                    }

                case MainMenu.SSPower:
                    {
                        str.Add("    Battery");
                        str.Add("    Solar Panel");
                        str.Add("    Small Reactor");
                        str.Add("    Large Reactor");
                        break;
                    }

                case MainMenu.SSThrusters:
                    {
                        str.Add("    Hydrogen Thruster");
                        str.Add("    Large Hydrogen Thruster");
                        str.Add("    Atmospheric Thruster");
                        str.Add("    Large Atmospheric Thruster");
                        str.Add("    Ion Thruster");
                        str.Add("    Large Ion Thruster");
                        break;
                    }

                case MainMenu.SSWheels:
                    {
                        str.Add("    Wheels");
                        str.Add("    Wheels 5x5");
                        str.Add("    Wheels 1x1");
                        str.Add("    Wheels Suspension");
                        str.Add("    Wheels Suspension 1x1");
                        str.Add("    Wheels Suspension 5x5");
                        break;
                    }

                case MainMenu.SSInternals:
                    {
                        str.Add("    Gyroscope");
                        str.Add("    Ore Detector");
                        str.Add("    Antenna");
                        str.Add("    Beacon");
                        str.Add("    Laser Antenna");
                        str.Add("    Projector");
                        break;
                    }

                case MainMenu.SSCargo:
                    {
                        str.Add("    Small Cargo Container");
                        str.Add("    Medium Cargo Container");
                        str.Add("    Large Cargo Container");
                        break;
                    }

                case MainMenu.SSWeapons:
                    {
                        str.Add("    Gatling Turret");
                        str.Add("    Missile Turret");
                        str.Add("    Gatling Gun");
                        str.Add("    Rocket Launcher");
                        str.Add("    Reloadable Rocket Launcher");
                        str.Add("    Warhead");
                        break;
                    }

                case MainMenu.SSTools:
                    {
                        str.Add("    Welder");
                        str.Add("    Grinder");
                        str.Add("    Drill");
                        break;
                    }

                case MainMenu.SSBlastDoors:
                    {
                        str.Add("    Blast Door");
                        str.Add("    Blast Door Corner");
                        str.Add("    Blast Door CornerInv");
                        str.Add("    Blast Door Edge");
                        break;
                    }

                case MainMenu.SSLightsDecoy:
                    {
                        str.Add("    Decoy");
                        str.Add("    Interior Light");
                        str.Add("    Spotlight");
                        break;
                    }

                case MainMenu.SSFunctional:
                    {
                        str.Add("    Programmable Block");
                        str.Add("    Control Panel");
                        str.Add("    Camera");
                        str.Add("    Sound Block");
                        str.Add("    Sensor");
                        str.Add("    Timer Block");
                        str.Add("    Button Panel");
                        break;
                    }

                case MainMenu.SSDisplays:
                    {
                        str.Add("    LCD Panel");
                        str.Add("    Wide LCD Panel");
                        str.Add("    Text Panel");
                        break;
                    }

                case MainMenu.SSConveyors:
                    {
                        str.Add("    Conveyor Tube");
                        str.Add("    Conveyor Junction");
                        str.Add("    Curved Conveypr Tube");
                        str.Add("    Connector");
                        str.Add("    Ejector");
                        str.Add("    Collector");
                        str.Add("    Conveyor Sorter");
                        break;
                    }

                case MainMenu.SSMechanical:
                    {
                        str.Add("    Landing Gear");
                        str.Add("    Piston");
                        str.Add("    Advanced Rotor");
                        str.Add("    Merge Block");
                        str.Add("    Rotor Part");
                        str.Add("    Advanced Rotor Part");
                        str.Add("    Air Vent");
                        str.Add("    Oxygen Generator");
                        str.Add("    Oxygen Tank");
                        str.Add("    Hydrogen Tank");
                        str.Add("    Artificial Mass");
                        str.Add("    Space Ball");
                        break;
                    }

                case MainMenu.LSMain:
                    {
                        str.Add("    Armor");
                        str.Add("    Cockpits");
                        str.Add("    Power");
                        str.Add("    Medical");
                        str.Add("    Thrusters");
                        str.Add("    Wheels");
                        str.Add("    Internals");
                        str.Add("    Refining");
                        str.Add("    Projector");
                        str.Add("    Cargo Containers");
                        str.Add("    Weapons");
                        str.Add("    Tools");
                        str.Add("    Doors");
                        str.Add("    Interior");
                        str.Add("    Lights/Decoy");
                        str.Add("    Functional");
                        str.Add("    Displays");
                        str.Add("    Conveyors");
                        str.Add("    Mechanical");
                        str.Add("    Windows");
                        str.Add("    Quantum");
                        break;
                    }

                case MainMenu.LSArmor:
                    {
                        str.Add("    Light Armor");
                        str.Add("    Heavy Armor");
                        break;
                    }

                case MainMenu.LSLightArmor:
                    {
                        str.Add("    Light Armor Block");
                        str.Add("    Light Armor Slope");
                        str.Add("    Light Armor Corner");
                        str.Add("    Light Armor Inv Corner");
                        str.Add("    Interior Wall");
                        str.Add("    Round Armor Slope");
                        str.Add("    Round Armor Corner");
                        str.Add("    Round Armor Inv Corner");
                        str.Add("    Light Armor Slope 2x1x1 Base");
                        str.Add("    Light Armor Slope 2x1x1 Tip");
                        str.Add("    Light Armor Corner 2x1x1 Base");
                        str.Add("    Light Armor Corner 2x1x1 Tip");
                        str.Add("    Light Armor Inv Corner 2x1x1 Base");
                        str.Add("    Light Armor Inv Corner 2x1x1 Tip");
                        break;
                    }

                case MainMenu.LSHeavyArmor:
                    {
                        str.Add("    Heavy Armor Block");
                        str.Add("    Heavy Armor Slope");
                        str.Add("    Heavy Armor Corner");
                        str.Add("    Heavy Armor Inv Corner");
                        str.Add("    Heavy Armor Round Slope");
                        str.Add("    Heavy Armor Round Corner");
                        str.Add("    Heavy Armor RoundInv Corner");
                        str.Add("    Heavy Armor Slope 2x1x1 Base");
                        str.Add("    Heavy Armor Slope 2x1x1 Tip");
                        str.Add("    Heavy Armor Corner 2x1x1 Base");
                        str.Add("    Heavy Armor Corner 2x1x1 Tip");
                        str.Add("    Heavy Armor Inv Corner 2x1x1 Base");
                        str.Add("    Heavy Armor Inv Corner 2x1x1 Tip");
                        break;
                    }

                case MainMenu.LSCockpit:
                    {
                        str.Add("    Control Station");
                        str.Add("    Flight Seat");
                        str.Add("    Cockpit");
                        str.Add("    Remote Control");
                        str.Add("    Passenger Seat");
                        break;
                    }

                case MainMenu.LSPower:
                    {
                        str.Add("    Battery");
                        str.Add("    Solar Panel");
                        str.Add("    Small Reactor");
                        str.Add("    Large Reactor");
                        break;
                    }

                case MainMenu.LSMedical:
                    {
                        str.Add("    Medical Room");
                        str.Add("    Cryo Chamber");
                        break;
                    }

                case MainMenu.LSThrusters:
                    {
                        str.Add("    Hydrogen Thruster");
                        str.Add("    Large Hydrogen Thruster");
                        str.Add("    Atmospheric Thruster");
                        str.Add("    Large Atmospheric Thruster");
                        str.Add("    Ion Thruster");
                        str.Add("    Large Ion Thruster");
                        break;
                    }

                case MainMenu.LSWheels:
                    {
                        str.Add("    Wheels");
                        str.Add("    Wheels 5x5");
                        str.Add("    Wheels 1x1");
                        str.Add("    Wheels Suspension");
                        str.Add("    Wheels Suspension 1x1");
                        str.Add("    Wheels Suspension 5x5");
                        break;
                    }

                case MainMenu.LSInternals:
                    {
                        str.Add("    Gyroscope");
                        str.Add("    Ore Detector");
                        str.Add("    Antenna");
                        str.Add("    Beacon");
                        str.Add("    Laser Antenna");
                        break;
                    }

                case MainMenu.LSRefining:
                    {
                        str.Add("    Refinery");
                        str.Add("    Assembler");
                        str.Add("    Productivity Module");
                        str.Add("    Effectiveness Module");
                        str.Add("    Productivity Module");
                        str.Add("    Power Efficiency Module");
                        str.Add("    Arc Furnace");
                        break;
                    }

                case MainMenu.LSProjector:
                    {
                        str.Add("    Projector");
                        break;
                    }

                case MainMenu.LSCargo:
                    {
                        str.Add("    Small Cargo Container");
                        str.Add("    Large Cargo Container");
                        break;
                    }

                case MainMenu.LSWeapons:
                    {
                        str.Add("    Interior Turret");
                        str.Add("    Gatling Turret");
                        str.Add("    Missile Turret");
                        str.Add("    Rocket Launcher");
                        str.Add("    Warhead");
                        break;
                    }

                case MainMenu.LSTools:
                    {
                        str.Add("    Welder");
                        str.Add("    Grinder");
                        str.Add("    Drill");
                        break;
                    }

                case MainMenu.LSDoors:
                    {
                        str.Add("    Sliding Door");
                        str.Add("    Door");
                        str.Add("    Airtight Hangar Door");
                        str.Add("    Blast Door");
                        str.Add("    Blast Door Corner");
                        str.Add("    Blast Door Corner Inv");
                        str.Add("    Blast Door Edge");
                        break;
                    }

                case MainMenu.LSInterior:
                    {
                        str.Add("    Cover WEall");
                        str.Add("    Half Cover Wall");
                        str.Add("    Steel Catwalk");
                        str.Add("    Steel Catwalk Two Sides");
                        str.Add("    Steel Catwalk Corner");
                        str.Add("    Steel Catwalk Plate");
                        str.Add("    Stairs");
                        str.Add("    Interior Pillar");
                        str.Add("    Vertical Window");
                        str.Add("    Diagonal Window");
                        str.Add("    Passage");
                        break;
                    }

                case MainMenu.LSLightsDecoy:
                    {
                        str.Add("    Decoy");
                        str.Add("    Interior Light");
                        str.Add("    Spotlight");
                        break;
                    }

                case MainMenu.LSFunctional:
                    {
                        str.Add("    Programmable Block");
                        str.Add("    Control Panel");
                        str.Add("    Camera");
                        str.Add("    Sound Block");
                        str.Add("    Sensor");
                        str.Add("    Timer Block");
                        str.Add("    Button Panel");
                        break;
                    }

                case MainMenu.LSDisplays:
                    {
                        str.Add("    LCD Panel");
                        str.Add("    Wide LCD Panel");
                        str.Add("    Text Panel");
                        break;
                    }

                case MainMenu.LSConveyors:
                    {
                        str.Add("    Conveyor Tube");
                        str.Add("    Conveyor Junction");
                        str.Add("    Curved Conveypr Tube");
                        str.Add("    Connector");
                        str.Add("    Collector");
                        str.Add("    Conveyor Sorter");
                        break;
                    }

                case MainMenu.LSMechanical:
                    {
                        str.Add("    Landing Gear");
                        str.Add("    Piston");
                        str.Add("    Advanced Rotor");
                        str.Add("    Merge Block");
                        str.Add("    Rotor Part");
                        str.Add("    Advanced Rotor Part");
                        str.Add("    Air Vent");
                        str.Add("    Oxygen Generator");
                        str.Add("    Oxygen Tank");
                        str.Add("    Hydrogen Tank");
                        str.Add("    Oxygen Farm");
                        break;
                    }

                case MainMenu.LSWindows:
                    {
                        str.Add("    Window 1x1 Flat");
                        str.Add("    Window 1x1 Flat Inv");
                        str.Add("    Window 1x1 Slope");
                        str.Add("    Window 1x2 Flat");
                        str.Add("    Window 1x2 Flat Inv");
                        str.Add("    Window 1x2 Slope");
                        str.Add("    Window 1x2 Side Left");
                        str.Add("    Window 1x2 Side Right");
                        str.Add("    Window 1x1 Face");
                        str.Add("    Window 1x1 Side");
                        str.Add("    Window 1x1 Inv");
                        str.Add("    Window 1x2 Face");
                        str.Add("    Window 1x2 Inv");
                        str.Add("    Window 2x3 Flat");
                        str.Add("    Window 2x3 Flat Inv");
                        str.Add("    Window 3x3 Flat");
                        str.Add("    Window 3x3 Flat Inv");
                        break;
                    }

                case MainMenu.LSQuantum:
                    {
                        str.Add("    Gravity Generator");
                        str.Add("    Spherical Gravity Generator");
                        str.Add("    Artificial Mass");
                        str.Add("    Space Ball");
                        str.Add("    Jump Drive");
                        break;
                    }
            }

            main_menu_max = str.Count;

            ResetQuant();
            DispMainMenu(str);
        }

        private void DispMainMenu(List<string> str)
        {
            var disp = GetControl("LCD Panel Menu") as IMyTextPanel;

            int loop = 0;
            int last = 0;
            int idx = 0;
            int caret = main_menu_pos;

            if (main_menu_pos >= main_menu_max)
                main_menu_pos = 0;

            if (main_menu_pos > 14)
            {
                loop = (main_menu_pos / 15) * 15;
                caret = loop + (main_menu_pos - loop);
                last = loop + 15;

                if (last > main_menu_max)
                    last = main_menu_max;
            }
            else
                last = (main_menu_max > 15) ? 15 : main_menu_max;

            for (bool i = true; loop < last; ++loop, ++idx, i = false)
            {
                string pre;

                if (loop == caret)
                    pre = ">";
                else
                    pre = "  ";

                WriteDisp(disp, pre + str[loop] + "\n", i);
            }

            if (idx < 17)
            {
                int max = 16 - idx;

                for (int i = 0; i < max; ++i)
                {
                    WriteDisp(disp, "\n");
                }

                WriteDisp(disp, "Buttons:\n");
                WriteDisp(disp, "1 - Up, 2 - Down, 3 - Select, 4 - Back");
            }
        }

        private void QuantButtons(string argument)
        {
            if (argument == "button_quant_1")
            {
                if (++quant_ones > 9)
                    quant_ones = 0;
            }
            else if (argument == "button_quant_2")
            {
                if (++quant_tens > 9)
                    quant_tens = 0;
            }
            else if (argument == "button_quant_3")
            {
                if (++quant_hundreds > 9)
                    quant_hundreds = 0;
            }
            else if (argument == "button_quant_4")
            {
                if (++quant_thousands > 9)
                    quant_thousands = 0;
            }

            UpdateVolDisp();
        }

        private void UpdateVolDisp()
        {
            var disp = GetControl("LCD Panel Quantity") as IMyTextPanel;

            WriteDisp(disp, "Quantity:\n\n\n\n\n", true);

            WriteDisp(disp, "                     " + N2T(quant_ones));
            WriteDisp(disp, "      " + N2T(quant_tens));
            WriteDisp(disp, "       " + N2T(quant_hundreds));
            WriteDisp(disp, "       " + N2T(quant_thousands) + "\n");

            WriteDisp(disp, "                  |___|  |___|  |___|  |___|\n");
            WriteDisp(disp, "                     |        |        |        |\n");
            WriteDisp(disp, "Button 1-------|        |        |        |\n");
            WriteDisp(disp, "Button 2--------------        |        |\n");
            WriteDisp(disp, "Button 3---------------------         |\n");
            WriteDisp(disp, "Button 4-----------------------------\n");
        }

        private string N2T(int num)
        {
            switch (num)
            {
                case 1: return "1 ";
                case 2: return "2";
                case 3: return "3";
                case 4: return "4";
                case 5: return "5";
                case 6: return "6";
                case 7: return "7";
                case 8: return "8";
                case 9: return "9";
            }

            return "0";
        }

        private void WriteDisp(IMyTextPanel disp, string text, bool init = false)
        {
            disp.WritePublicText(text, !init);
        }

        private int GetQuantity()
        {
            return ((quant_thousands * 1000) + (quant_hundreds * 100) + (quant_tens * 10) + (quant_ones));
        }

        private void AddBatch(FactoryBlockType bt, int num, FactoryComponents fc)
        {
            if (GetQuantity() > 0)
            {
                var newcomp = new FactoryComponents();

                switch (bt)
                {
                    case FactoryBlockType.SSLightArmorBlock:
                        {
                            break;
                        }
                }

                fc.Add(newcomp, GetQuantity());
            }
        }
    }
}